## Zerologon

С конфигурацией сети в GNS3 я решил не заморачиваться, потому будем поднимать `windows server 2016` в виртуалбоксе и атаковать его с хостовой машины.

![](./img/server-ip.png)

Атакуемый сервер будет жить на IP `192.168.0.104`.

Для иммитации атаки используем скрипт из https://github.com/SecuraBV/CVE-2020-1472.

![](./img/attack.png)

Скрипт не проводит никакой пост-эксплуатации, но нам это и не нужно.

Посмотрим, как атака отразилась в файле `netlogon`:

![](./img/netlogon.png)

[Всё так, как и должно быть](https://habr.com/ru/company/bizone/blog/526168/)

Также можно настроить политики аудита, которые могут помочь выявить действия злоумышленника:

![](./img/audit.png)

Но конкретно в нашем случае мы по ним ничего не увидим, потому что нет пост-эксплуатации.